// React
import {createRoot} from 'react-dom/client';
import App from './App';
import Home from './pages/Home'
import {Wallet} from './near-wallet';
import {Box, ChakraProvider, Text} from '@chakra-ui/react'

import {Router} from './router';
import Navbar from './layout/Navbar';
import {BrowserRouter as Router, Routes, Route, Link} from "react-router-dom"
import BlogArticle from './pages/BlogArticle';
import Details from './pages/Details';
import MyContent from './pages/MyContent';
import Profile from './pages/Profile';
import AllCourses from './pages/AllCourses';
import CreateNewContent from './pages/CreateNewContent';
import Subscriptions from './pages/Subscriptions';
import Earnings from './pages/Earnings';
import Swal from 'sweetalert2';
import {useState} from 'react';
// When creating the wallet you can optionally ask to create an access key
// Having the key enables to call non-payable methods without interrupting the user to sign
const wallet = new Wallet({createAccessKeyFor: CONTRACT_ADDRESS})
const container = document.getElementById('root');
const root = createRoot(container); // createRoot(container!) if you use TypeScript

const CONTRACT_ADDRESS = 'dev-1678298009114-37643504401882'

// Setup on page load
window.onload = async () => {
    const isSignedIn = await wallet.startUp()

    async function getProfile() {
        try {
            return await wallet.viewMethod({
                CONTRACT_ADDRESS,
                method: 'get_profile',
                args: {account_id: wallet.accountId}
            })
        } catch (e) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: `No hay un perfil autenticado`,
            })
        }

        //console.log('Profile', profile)
    }

    root.render(
        // <App isSignedIn={isSignedIn} contractId={CONTRACT_ADDRESS} wallet={wallet} />

        <ChakraProvider>
            <Router>
                <Navbar
                    isSignedIn={isSignedIn}
                    contractId={CONTRACT_ADDRESS}
                    wallet={wallet}
                    profile={getProfile}
                />

                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/blogarticle"
                           element={<BlogArticle isSignedIn={isSignedIn} contractId={CONTRACT_ADDRESS}
                                                 wallet={wallet}/>}/>
                    <Route path="/details"
                           element={<Details isSignedIn={isSignedIn} contractId={CONTRACT_ADDRESS} wallet={wallet}/>}/>
                    <Route path="/mycontent" element={<MyContent isSignedIn={isSignedIn} contractId={CONTRACT_ADDRESS}
                                                                 wallet={wallet}/>}/>
                    <Route path="/profile"
                           element={<Profile isSignedIn={isSignedIn} contractId={CONTRACT_ADDRESS} wallet={wallet}/>}/>
                    <Route path="/allcourses" element={<AllCourses isSignedIn={isSignedIn} contractId={CONTRACT_ADDRESS}
                                                                   wallet={wallet}/>}/>
                    <Route path="/subscriptions"
                           element={<Subscriptions isSignedIn={isSignedIn} contractId={CONTRACT_ADDRESS} wallet={wallet}
                                                   profile={getProfile}/>}/>
                    <Route path="/earnings"
                           element={<Earnings isSignedIn={isSignedIn} contractId={CONTRACT_ADDRESS} wallet={wallet}/>}/>
                </Routes>
            </Router>
            <Box bg={'white'} h={'900'}>
                    
            </Box>
        </ChakraProvider>
    );
}