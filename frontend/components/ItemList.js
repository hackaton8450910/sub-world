import React, { useState, useEffect } from "react";
import { Box, Flex, Text, Spacer, HStack } from "@chakra-ui/react";
import CourseCard from "./CourseCard";

function ItemList({ items, curPage, itemLimit, isSignedIn, contractId, wallet }) {
  const [contents, setContents] = useState([]);

  useEffect(() => {
    const offset = curPage * itemLimit - itemLimit;
    const getList = (curPage, itemLimit) => {
      setContents(items.slice(offset, offset + itemLimit));
    };

    getList(curPage, itemLimit);
  }, [curPage, itemLimit, items]);

  return (
    <HStack borderWidth={2} >
      {contents.map((content,index) => {
        return (
          <Flex key={index}  >
            <CourseCard content={content} isSignedIn={isSignedIn} contractId={contractId} wallet={wallet} />
          </Flex>
        )
      })}
    </HStack>
  );
}

export default ItemList;