import {
    Heading,
    Avatar,
    Box,
    Center,
    Text,
    Stack,
    Button,
    Link,
    Badge,
    useColorModeValue,
    Input,
    Select,
} from '@chakra-ui/react';
import {useEffect, useState} from 'react';
import Swal from 'sweetalert2';

export default function ProfileCard({isSignedIn, contractId, wallet}) {

    const [handle, setHandle] = useState('');
    const [type, setType] = useState('')
    const [isLoading, setIsLoading] = useState(false)


    async function checkHandle(handle = 'test') {
        return await wallet.viewMethod({contractId, method: 'handle_exists', args: {handle}})
    }

    async function getAllUsers() {
        let res = await wallet.viewMethod({contractId, method: 'get_users'})
        console.log('Users: ', res)
    }

    async function get_all_courses() {
        let res = await wallet.viewMethod({contractId, method: 'get_all_courses'})
        console.log('Users: ', res)
    }

    async function createNewUser() {

        setIsLoading(true)

        const newUser = {
            handle,
            type
        }

        let handleExists = await checkHandle(newUser.handle)

        if (handleExists) {
            alert('No se puede crear el usuario con ese handle')
            setIsLoading(false)
            return
        }

        const res = await wallet.callMethod({
            contractId,
            method: 'create_new_user',
            args: newUser
        })
        setIsLoading(false)
        console.log('done', res)
        await getAllUsers()
    }

    useEffect(() => {
        getProfile()
    }, [])

    async function getProfile() {


        try {
            const profile = await wallet.viewMethod({
                contractId,
                method: 'get_profile',
                args: {account_id: wallet.accountId}
            })
            setHandle(profile.handle)
            setType(profile.type)
        } catch (err) {
            console.log(err)
        }
        //console.log('Profile', profile)
    }

    return (
        <Center py={6}>
            <Box

                w={'5/6'}
                bg={useColorModeValue('white', 'gray.900')}
                boxShadow={'2xl'}
                rounded={'lg'}
                p={6}
                textAlign={'center'}>
                <Avatar
                    size={'xl'}
                    src={
                        'https://images.unsplash.com/photo-1520810627419-35e362c5dc07?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&ixid=eyJhcHBfaWQiOjE3Nzg0fQ'
                    }
                    alt={'Avatar Alt'}
                    mb={4}
                    pos={'relative'}
                    _after={{
                        content: '""',
                        w: 4,
                        h: 4,
                        bg: 'green.300',
                        border: '2px solid white',
                        rounded: 'full',
                        pos: 'absolute',
                        bottom: 0,
                        right: 3,
                    }}
                />
                <Heading textAlign={'left'} size={'md'} p={100}>Crea y consume todo el contenido, sin darnos tus
                    datos,
                    tu privacidad como debe de ser: ¡Tuya!</Heading>
                <Text textAlign={'left'} color={'gray.400'}>Handle (Optional)</Text>
                <Input fontSize={'xl'} placeholder={'Handle id'} value={handle}
                       onChange={(e) => setHandle(e.target.value)}/>
                <Text textAlign={'left'} color={'gray.400'}>Elegir un tipo de contribuidor</Text>
                <Select value={type} placeholder='Selecciona...' onChange={(e) => setType(e.target.value)}>
                    <option value='creator'>Creador</option>
                    <option value='user'>Usuario</option>
                </Select>

                <Stack mt={8} direction={'row'} spacing={4}>

                    <Button
                        flex={1}
                        fontSize={'sm'}
                        rounded={'full'}
                        bg={'blue.400'}
                        color={'white'}
                        boxShadow={
                            '0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)'
                        }
                        _hover={{
                            bg: 'blue.500',
                        }}
                        _focus={{
                            bg: 'blue.500',
                        }}
                        onClick={createNewUser}
                        isLoading={isLoading}
                    >
                        Guardar
                    </Button>
                </Stack>
            </Box>
        </Center>
    );
}