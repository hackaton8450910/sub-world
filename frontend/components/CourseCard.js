import { useState } from 'react';
import {
  Box,
  Heading,
  Text,
  Img,
  Flex,
  Center,
  useColorModeValue,
  HStack,
  Button,
} from '@chakra-ui/react';
import { BsArrowUpRight, BsHeartFill, BsHeart, BsFillTrashFill } from 'react-icons/bs';
import Swal from 'sweetalert2';

export default function CourseCard({ content, isSignedIn, contractId, wallet }) {

  async function deleteContent(id) {
    try {
    const res = await wallet.callMethod({
      contractId,
      method: 'delete_content',
      args: { id }
    })
  }
  catch(e){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: `Something went wrong!: ${e}`,
    })
  }
  }
  return (
    <Box py={6} px={6}>
      <Box
        w="xs"
        rounded={'md'}
        my={5}
        mx={[0, 5]}
        overflow={'hidden'}
        bg="white"
      >
        <Box h={'200px'} borderBottom={'1px'} borderColor="black">
          <Img
            src={content.pictures[0]}
            roundedTop={'sm'}
            objectFit="cover"
            h="full"
            w="full"
            alt={'Blog Image'}
          />
        </Box>
        <Box p={4}>
          <Box
            bg="#f9b233"
            display={'inline-block'}
            px={2}
            py={1}
            color="white"
            rounded={'md'}
            mb={2}>
            <Text fontSize={'xs'} fontWeight="medium">
              ${content.price}
            </Text>
          </Box>
          <Heading color={'black'} fontSize={'2xl'} noOfLines={1}>
            {content.title}
          </Heading>
          <Text color={'gray.500'} noOfLines={2}>
            {content.description}
          </Text>
        </Box>
        <HStack>
          <Flex
            py={2}
            px={4}
            bg={'#406782'}
            
            justifyContent={'space-between'}
            rounded={'lg'}
            cursor={'pointer'}
          >
            <Text fontSize={'md'} color={'white'} fontWeight={'semibold'}>
              Detalles
            </Text>

          </Flex>
          <Flex
            p={4}
            
            justifyContent={'space-between'}
            roundedBottom={'sm'}

            cursor="pointer">
            <Button onClick={() => deleteContent(content.id )}><BsFillTrashFill fontSize={'24px'} /></Button>

          </Flex>
        </HStack>
      </Box>
    </Box>
  );
}