import { ReactNode, useEffect, useState } from 'react';
import {
    Tag,
    Box,
    Flex,
    Avatar,
    HStack,
    Link,
    IconButton,
    Button,
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    MenuDivider,
    useDisclosure,
    useColorModeValue,
    Stack,
} from '@chakra-ui/react';
import { Link, useNavigate } from "react-router-dom"
import { HamburgerIcon, CloseIcon } from '@chakra-ui/icons';
import { connect, keyStores } from "near-api-js";
import { getNearUsdValue } from "../utils";
import Swal from 'sweetalert2';

const Links = [{
    name:'Inicio',
    type:'',
    router:'/'
},
{
    name:'Blogarticle',
    type:'',
    router:'blogarticle'
},
{
    name:'Detalles',
    type:'',
    router:'details'
},
{
    name:'Contenido',
    type:'',
    router:'allcourses'
},
{
    name:'Mi contenido',
    type:'creator',
    router:'mycontent'
},
{
    name:'Perfil',
    type:'',
    router:'profile'
},
{
    name:'Suscripciones',
    type:'creator',
    router:'subscriptions'
},
{
    name:'Ganancias',
    type:'creator',
    router:'earnings'
}
]
const LinksUser= Links.filter(item=>item.type!=='creator')
export default function Navbar({ isSignedIn, contractId, wallet }) {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [balance, setBalance] = useState(0)
    const [handle, setHandle] = useState()
    const [type, setType] = useState()
    async function getProfile() {


        try {
            const profile = await wallet.viewMethod({
                contractId,
                method: 'get_profile',
                args: { account_id: wallet.accountId }
            })
            setHandle(profile.handle)
            setType(profile.type)
            console.log(LinksUser)
        } catch (err) {
            console.log(err)
        }
        //console.log('Profile', profile)
    }


    async function getAccountBalance() {
        const connectionConfig = {
            networkId: "testnet",
            keyStore: new keyStores.BrowserLocalStorageKeyStore(),
            nodeUrl: "https://rpc.testnet.near.org",
            walletUrl: "https://wallet.testnet.near.org",
            helperUrl: "https://helper.testnet.near.org",
            explorerUrl: "https://explorer.testnet.near.org",
        };

        const nearConnection = await connect(connectionConfig);
        const account = await nearConnection.account(wallet.accountId);
        const balance = await account.getAccountBalance();
        const usdBalance = await getNearUsdValue(balance.available)

        setBalance(usdBalance)
    }

    useEffect(() => {
        getProfile()
        getAccountBalance()
        
    })

    const navigate = useNavigate();


    async function logOut() {
        try {
            await wallet.signOut()
        } catch (e) {
            Swal.fire({
                icon: 'error',
                title: 'Atención',
                text: `Hubo un error al cerrar sesión, inténtalo de nuevo más tarde. Código:  ${e}`,
            })
        }
    }

    async function logIn() {
        try {
            await wallet.signIn()
        } catch (e) {
            Swal.fire({
                icon: 'error',
                title: 'Atención',
                text: `Hubo un error al iniciar sesión, verifique su equipo o su proveedor de wallets e inténtalo de nuevo. 
                Código:   ${e}`,
            })
        }


        navigate('/profile')
    }

    return (
        <>
            <Box bg={'white'} px={4}>
                <Flex h={16} alignItems={'center'} justifyContent={'space-between'}>
                    <IconButton
                        size={'md'}
                        icon={isOpen ? <CloseIcon /> : <HamburgerIcon />}
                        aria-label={'Open Menu'}
                        display={{ md: 'none' }}
                        onClick={isOpen ? onClose : onOpen}
                    />
                    <HStack spacing={8} alignItems={'center'}>
                        <HStack
                            as={'nav'}
                            spacing={4}
                            display={{ base: 'none', md: 'flex' }}>


                            {type === 'creator' ?Links.map((link, index) => (
                                <Link
                                    key={index}
                                    px={2}
                                    py={1}
                                    rounded={'md'}
                                    _hover={{
                                        textDecoration: 'none',
                                        bg: useColorModeValue('gray.200', 'gray.700'),
                                    }}
                                    to={link.router}
                                >
                                    {link.name}
                                </Link> 
                            )):LinksUser.map((link, index) => (
                                <Link
                                key={index}
                                px={2}
                                py={1}
                                rounded={'md'}
                                _hover={{
                                    textDecoration: 'none',
                                    bg: useColorModeValue('gray.200', 'gray.700'),
                                }}
                                to={link.router}
                            >
                                {link.name}
                            </Link>  
                            ))}
                        </HStack>
                    </HStack>
                    <Flex alignItems={'center'}>
                        <Menu>
                            <Tag colorScheme='blue' mr={2}>{balance.toFixed(2)} USD</Tag>

                            {
                                !isSignedIn
                                    ? <Button bg={'#000000'} textColor={'#ffffff'} onClick={logIn}>Conectar
                                        Wallet</Button>
                                    :
                                    <Button bg={'#000000'} textColor={'#ffffff'} onClick={logOut}>Cerrar
                                        sesión</Button>
                            }

                            <MenuButton
                                as={Button}
                                rounded={'full'}
                                variant={'link'}
                                cursor={'pointer'}
                                ml={3}
                                minW={0}>
                                <Avatar
                                    size={'sm'}
                                    src={
                                        'https://images.unsplash.com/photo-1493666438817-866a91353ca9?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=b616b2c5b373a80ffc9636ba24f7a4a9'
                                    }
                                />
                            </MenuButton>
                            <MenuList>
                                <MenuItem>Link 1</MenuItem>
                                <MenuItem>Link 2</MenuItem>
                                <MenuDivider />
                                <MenuItem>Link 3</MenuItem>
                            </MenuList>
                        </Menu>
                    </Flex>
                </Flex>

                {isOpen ? (
                    <Box pb={4} display={{ md: 'none' }}>
                        <Stack as={'nav'} spacing={4}>
                            {Links.map((link) => (
                                <Link
                                    px={2}
                                    py={1}
                                    rounded={'md'}
                                    _hover={{
                                        textDecoration: 'none',
                                        bg: useColorModeValue('gray.200', 'gray.700'),
                                    }}
                                    href={'#'}>
                                    {link.name}
                                </Link>
                            ))}
                        </Stack>
                    </Box>
                ) : null}
            </Box>
        </>
    );
}