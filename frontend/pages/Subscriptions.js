import React, { useEffect, useState } from 'react'
import {
    List,
    ListItem,
    ListIcon,
    OrderedList,
    UnorderedList,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    useDisclosure,
    Button,
    Text,
    Box,
    Stack,
    HStack,
    FormControl,
    FormLabel,
    Input,
} from '@chakra-ui/react'
import Swal from 'sweetalert2'
import { profile } from 'console'
import { BsFillTrashFill } from 'react-icons/bs'

function Subscriptions({ isSignedIn, contractId, wallet }) {
    const [name, setName] = useState('')
    const [durationInMonths, setDurationInMonths] = useState(0)
    const [price, setPrice] = useState(0)
    const [subscriptions, setSubscriptions] = useState([])
    const [isLoading, setIsLoading] = useState(false)



    const { isOpen, onOpen, onClose } = useDisclosure()

    useEffect(() => {
        getSubscriptions()
        let params= new URL(window.location)
        if(params.searchParams.get('transactionHashes')){
            Swal.fire({
                icon: 'success',
                title: 'Transacción exitosa!',
              })
        }
    }, [])
    async function createSubscription() {
        setIsLoading(true)
        if (isSignedIn) {
            try {
                const res = await wallet.callMethod({
                    contractId,
                    method: 'create_new_subscription',
                    args: { name, price, durationInMonths }
                })
                console.log('Nueva suscripcion: ', res)
            } catch (e) {
                alert('Hubo un error al crear la suscripcion')
                setIsLoading(false)
            }
        }
        else {
            alert('Parece que no tienes permiso de crear suscripciones')
            setIsLoading(false)
        }
        setIsLoading(false)
    }

    async function getSubscriptions() {
        try {
            const res = await wallet.viewMethod({
                contractId,
                method: 'get_creator_subscriptions',
                args: { account_id: wallet.accountId }
            })

            console.log('Suscripciones: ', res)
            if (!res) {
                Swal.fire({
                    icon: 'error',
                    title: 'Atención',
                    text: `No hay suscripciones disponibles, ¿desea crear una nueva suscripción?
                `,
                })
            } else {
                setSubscriptions(res)

            }
        } catch {
            alert('No pudimos suscribirte')
        }

    }
    async function deleteSubscription(id) {
        try {
            const res = await wallet.callMethod({
              contractId,
              method: 'delete_subscription',
              args: { id }
            })
          }
          catch(e){
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: `Something went wrong!: ${e}`,
            })
          }
          }
    return (
        <>
            <Box align={'right'} mt={5}>{isSignedIn?
                <Button onClick={onOpen} backgroundColor={'#406782'} color={'white'} >Nueva Suscripción</Button>
                : null}
            </Box>
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Crea una nueva suscripción</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Box
                            p={8}>
                            <Stack spacing={4}>
                                <HStack>
                                    <FormControl id="name" isRequired>
                                        <FormLabel>Nombre</FormLabel>
                                        <Input type="text" onChange={(e) => setName(e.target.value)} />
                                    </FormControl>
                                    <FormControl id="price" isRequired>
                                        <FormLabel>Precio</FormLabel>
                                        <Input type="number" onChange={(e) => setPrice(e.target.value)} />
                                    </FormControl>
                                </HStack>
                                <FormControl id="durationInMonths" isRequired>
                                    <FormLabel>Duración en meses</FormLabel>
                                    <Input type="number" onChange={(e) => setDurationInMonths(e.target.value)} />
                                </FormControl>
                            </Stack>
                        </Box>
                    </ModalBody>

                    <ModalFooter>
                        <Button variant='ghost' mr={3} onClick={onClose}>
                            Close
                        </Button>
                        <Button colorScheme='blue' isLoading={isLoading} onClick={createSubscription}>Guardar</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
            <UnorderedList w={'auto'} mt={5}>
                {subscriptions ? subscriptions.map((sub, index) => {
                    return <Box key={index}bg={'#406782'}rounded={'md'} p={6} shadow={'md'} my={5} mx={10} >
                        <Text fontWeight={'bold'} color={'white'}>{sub.name}</Text>
                        <Text color={'white'}>Precio: ${sub.price}</Text>
                        <Text fontWeight={'bold'} color={'white'}>Meses de suscripción: {sub.durationInMonths}</Text>
                        
                    </Box>

                }) : <Text>Aún no has creado suscripciones para tus seguidores</Text>}

            </UnorderedList>
        </>
    )
}

export default Subscriptions