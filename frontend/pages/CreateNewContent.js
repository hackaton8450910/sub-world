import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  HStack,
  InputRightElement,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
  Link,
  Textarea,
} from '@chakra-ui/react';
import { useState } from 'react';
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { WithContext as ReactTags } from 'react-tag-input';
import '../styles/styles.css'
import Swal from 'sweetalert2';
export default function CreateNewContent({ isSignedIn, contractId, wallet }) {

  const [tags, setTags] = useState([]);
  const [title, setTitle] = useState()
  const [description, setDescription] = useState()
  const [price, setPrice] = useState(0)

  async function createNewContent() {
    try {
      const res = await wallet.callMethod({
        contractId,
        method: 'create_new_content',
        args: {
          title,
          description,
          price,
          pictures: tags,

        }

      })
    } catch (e) {
      Swal.fire({
        icon: 'error',
        title: 'Atención',
        text: `Hubo un error al crear nuevo contenido`,
      })
    }
    console.log('Create Content', res)
  }
  return (
    <Flex
      minH={'100vh'}
      align={'center'}
      justify={'center'}
      bg={useColorModeValue('gray.50', 'gray.800')}>
      <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
        <Stack align={'center'}>
          <Heading fontSize={'2xl'} textAlign={'center'}>
            Crea un nuevo contenido
          </Heading>
          <Text fontSize={'lg'} color={'gray.600'}>
            y obtén los beneficios de Sub World
          </Text>
        </Stack>
        <Box
          rounded={'lg'}
          bg={useColorModeValue('white', 'gray.700')}
          boxShadow={'lg'}
          p={8}>
          <Stack spacing={4}>
            <HStack>
              <FormControl id="title" isRequired>
                <FormLabel>Título</FormLabel>
                <Input type="text" onChange={(e) => setTitle(e.target.value)} />
              </FormControl>
              <FormControl id="price" isRequired>
                <FormLabel>Precio</FormLabel>
                <Input type="number" onChange={(e) => setPrice(e.target.value)} />
              </FormControl>
            </HStack>
            <FormControl id="description" isRequired>
              <FormLabel>Descripción</FormLabel>
              <Textarea onChange={(e) => setDescription(e.target.value)} />
            </FormControl>
            <FormControl id="pictures" isRequired>
              <HStack>
                <FormLabel>Imagenes</FormLabel>
                <Button
                  size={'xs'}
                  colorScheme={'teal'}
                  onClick={() => {
                    const localTags = [...tags]
                    localTags.push('')
                    setTags(localTags)
                  }}
                >+</Button>

              </HStack>
              {
                tags.map((tag, index) => {
                  return <HStack mt={2} key={index}>
                    <Input value={tag} onChange={(e) => {
                      const localTags = [...tags]
                      localTags[index] = e.target.value
                      setTags(localTags)
                    }} />
                    <Button size={'sm'} colorScheme={'red'} onClick={() => {
                      const localTags = [...tags]
                      localTags.splice(index, 1)
                      setTags(localTags)
                    }}>
                      Eliminar
                    </Button>
                  </HStack>
                })
              }
            </FormControl>

            <Stack spacing={10} pt={2}>
              <Button
                loadingText="Submitting"
                size="lg"
                bg={'blue.400'}
                color={'white'}
                _hover={{
                  bg: 'blue.500',
                }}
                onClick={createNewContent}>
                Crear contenido
              </Button>
            </Stack>
          </Stack>
        </Box>
      </Stack>
    </Flex>
  );
}