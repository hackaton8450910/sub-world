import React from 'react'
import React, { useState, useEffect } from 'react';
import { Box, Button, Flex, FormControl, FormLabel, HStack, Input, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Spacer, Stack, Text, Textarea, useDisclosure } from '@chakra-ui/react';
import {
    Pagination,
    usePagination,
    PaginationNext,
    PaginationPage,
    PaginationPrevious,
    PaginationContainer,
    PaginationPageGroup,
} from '@ajna/pagination'
import { CgChevronLeft, CgChevronRight } from 'react-icons/cg';
import ItemList from '../components/ItemList';
import Swal from 'sweetalert2';
import { URLSearchParams } from 'url';


function MyContent({ isSignedIn, contractId, wallet }) {
    const [content, setContent] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [tags, setTags] = useState([]);
    const [title, setTitle] = useState()
    const [description, setDescription] = useState()
    const [price, setPrice] = useState(0)
    const { isOpen, onOpen, onClose } = useDisclosure()
    useEffect(() => {
        getUserContent()
        let params= new URL(window.location)
        if(params.searchParams.get('transactionHashes')){
            Swal.fire({
                icon: 'success',
                title: 'Transacción exitosa!',
              })
        }
    }, [])
    async function getUserContent() {
        let res = await wallet.viewMethod({ contractId, method: 'get_user_content', args: { account_id: wallet.accountId } })
        if (!res) {
            Swal.fire({
                icon: 'error',
                title: 'Atención',
                text: `No hay contenido disponible, ¿desea crear nuevo contenido?`,
            })
        } else {
            setContent(res)
        }
    }

    async function createNewContent() {
        setIsLoading(true)
        try {
            const res = await wallet.callMethod({
                contractId,
                method: 'create_new_content',
                args: {
                    title,
                    description,
                    price,
                    pictures: tags,
                }
            })
        } catch (e) {
            setIsLoading(false)
            Swal.fire({
                icon: 'error',
                title: 'Atención',
                text: `Hubo un error al crear nuevo contenido`,
            })
        }
        setIsLoading(false)
    }
    const itemLimit = 3;

    const normalstyles = {
        bg: 'white'
    };

    const activeStyles = {
        bg: 'blue.300'
    };

    const {
        currentPage,
        setCurrentPage,
        pagesCount,
        pages
    } = usePagination({
        pagesCount: Math.round(content.length / itemLimit),
        initialState: { currentPage: 1 },
    });
    return (
        <Box >
            <Box align={'right'} mt={5}>{isSignedIn /*&& profile.type==='creator'*/ ?
                <Button onClick={onOpen} backgroundColor={'#406782'} color={'white'} >Nuevo Contenido</Button>
                : null}
            </Box>
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Crea un nuevo contenido</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Box
                            p={8}>
                            <Stack spacing={4}>
                                <HStack>
                                    <FormControl id="title" isRequired>
                                        <FormLabel>Título</FormLabel>
                                        <Input type="text" onChange={(e) => setTitle(e.target.value)} />
                                    </FormControl>
                                    <FormControl id="price" isRequired>
                                        <FormLabel>Precio</FormLabel>
                                        <Input type="number" onChange={(e) => setPrice(e.target.value)} />
                                    </FormControl>
                                </HStack>
                                <FormControl id="description" isRequired>
                                    <FormLabel>Descripción</FormLabel>
                                    <Textarea onChange={(e) => setDescription(e.target.value)} />
                                </FormControl>

                                <FormControl id="pictures" isRequired>
                                    <HStack>
                                        <FormLabel>Imagenes</FormLabel>
                                        <Button
                                            size={'xs'}
                                            colorScheme={'teal'}
                                            onClick={() => {
                                                const localTags = [...tags]
                                                localTags.push('')
                                                setTags(localTags)
                                            }}
                                        >+</Button>

                                    </HStack>
                                    {
                                        tags.map((tag, index) => {
                                            return <HStack mt={2} key={index}>
                                                <Input value={tag} onChange={(e) => {
                                                    const localTags = [...tags]
                                                    localTags[index] = e.target.value
                                                    setTags(localTags)
                                                }} />
                                                <Button size={'sm'} colorScheme={'red'} onClick={() => {
                                                    const localTags = [...tags]
                                                    localTags.splice(index, 1)
                                                    setTags(localTags)
                                                }}>
                                                    Eliminar
                                                </Button>
                                            </HStack>
                                        })
                                    }
                                </FormControl>
                            </Stack>
                        </Box>
                    </ModalBody>

                    <ModalFooter>
                        <Button variant='ghost' mr={3} onClick={onClose}>
                            Close
                        </Button>
                        <Button colorScheme='blue' isLoading={isLoading} onClick={createNewContent}>Crear contenido</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
            {content.length !== 0 ?
                <ItemList items={content} curPage={currentPage} itemLimit={itemLimit} isSignedIn={isSignedIn} contractId={contractId} wallet={wallet} />
                : <Text mt={5}>Todavía no has subido contenido</Text>}
            <Flex p={2}>
                <Spacer />
                <Pagination
                    pagesCount={pagesCount}
                    currentPage={currentPage}
                    onPageChange={setCurrentPage}>
                    <PaginationContainer>
                        <PaginationPrevious bg="white">
                            <CgChevronLeft />
                        </PaginationPrevious>
                        <PaginationPageGroup>
                            {pages.map((page) => (
                                <PaginationPage
                                    key={`paginator_page_${page}`}
                                    page={page}
                                    normalStyles={normalstyles}
                                    activeStyles={activeStyles}
                                />
                            ))}

                        </PaginationPageGroup>
                        <PaginationNext bg="white">
                            <CgChevronRight />
                        </PaginationNext>
                    </PaginationContainer>
                </Pagination>
            </Flex>
        </Box>
    );
}

export default MyContent