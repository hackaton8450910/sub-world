import { Box, Button } from '@chakra-ui/react'
import React, { useEffect } from 'react'
import ProfileCard from '../components/ProfileCard'
import Statistics from '../components/Statistics'




function Profile({ isSignedIn, contractId, wallet }) {

    return (
        <Box>
            
            <ProfileCard isSignedIn={isSignedIn} contractId={contractId} wallet={wallet} />
            <Statistics />
        </Box>
    )
}

export default Profile