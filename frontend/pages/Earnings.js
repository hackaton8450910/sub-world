import {
    Flex, Box, FormControl, FormLabel, Input, HStack, Stack, Button, Heading, Text, useColorModeValue, Link, Textarea,
} from '@chakra-ui/react';
import {useEffect, useState} from 'react';
import BN from 'bn.js'
import '../styles/styles.css'
import {getStNearUsdValue, getUsdNeardValue} from "../utils";


export default function Earnings({isSignedIn, contractId, wallet}) {

    const [quantity, setQuantity] = useState()
    const [stacked, setStacked] = useState()
    const [quantityUnstake, setQuantityUnstake] = useState()
    const [conversion, setConversion] = useState()

    useEffect(() => {
        getStacked()
        setDolarNearConversion()
    })

    const metacontract = 'meta-v2.pool.testnet'
    async function setDolarNearConversion() {
        const res = await getUsdNeardValue(1)
        setConversion(res)
    }

    async function stake() {
        const amountYoctos = new BN(quantity +"0".repeat(24))

        wallet.callMethod({
            contractId: metacontract,
            method: 'deposit_and_stake',
            deposit: amountYoctos
        })
    }

    async function unStake() {
        const amountYoctos = new BN(quantityUnstake +"0".repeat(24))
        const minYoctos = new BN(quantityUnstake - 1 +"0".repeat(24))

        wallet.callMethod({
            contractId: metacontract,
            method: 'liquid_unstake',
            args: {
                st_near_to_burn: amountYoctos.toString(),
                min_expected_near: minYoctos.toString()
            }
        })
    }

    async function getStacked() {
        const balance = await wallet.viewMethod({
            contractId: metacontract,
            method: 'ft_balance_of',
            args: {
                account_id: wallet.accountId
            }
        })
        const result = await getStNearUsdValue(balance)
        setStacked(result)
    }

    return (<Flex
        minH={'50vh'}
        align={'center'}
        justify={'center'}
        bg={useColorModeValue('gray.50', 'gray.800')}>
        <Stack spacing={8} mx={'auto'} maxW={'lg'} py={12} px={6}>
            <Stack align={'center'}>
                <Heading fontSize={'2xl'} textAlign={'center'}>
                    Disfruta de tus ganancias
                </Heading>
                <Text fontSize={'lg'} color={'gray.600'}>
                    y obten mayores rendimientos
                </Text>
            </Stack>
            <Box
                rounded={'lg'}
                bg={useColorModeValue('white', 'gray.700')}
                boxShadow={'lg'}
                p={8}>
                <Stack spacing={4}>
                    <HStack>
                        <FormControl id="title" isRequired>
                            <FormLabel>Cantidad a stackear (NEAR)</FormLabel>
                            <Input type="number" onChange={(e) => setQuantity(e.target.value)}/>
                            {
                                quantity
                                    ? <Text fontSize={'xs'}>Dolares { (quantity / conversion).toFixed(2) }</Text>
                                    : null
                            }

                            <Button mt={2} onClick={stake}>Invertir</Button>
                        </FormControl>
                    </HStack>
                </Stack>
            </Box>
            <Box
                rounded={'lg'}
                bg={useColorModeValue('white', 'gray.700')}
                boxShadow={'lg'}
                p={8}>
                <Stack spacing={4}>
                    <HStack>
                        <FormControl id="title" isRequired>
                            {
                                stacked ? <FormLabel>Cantidad a retirar {stacked.toFixed(2)} stNEAR</FormLabel>
                                : <FormLabel>Cantidad a retirar </FormLabel>
                            }

                            <Input type="text" onChange={(e) => setQuantityUnstake(e.target.value)}/>
                            {
                                quantityUnstake
                                    ? <Text fontSize={'xs'}>Dolares { (quantityUnstake / conversion).toFixed(2) }</Text>
                                    : null
                            }
                            <Button mt={2} onClick={unStake}>Retirar</Button>
                        </FormControl>
                    </HStack>
                </Stack>
            </Box>
        </Stack>
    </Flex>);
}