import {NearBindgen, near, call, view, Vector} from 'near-sdk-js';
import {ContentModel} from "../models/ContentModel";
import {UserModel} from "../models/User";
import {SubscriptionModel} from "../models/SubscriptionModel";

@NearBindgen({})


class CreatorsContent {
    // Variable to save all the content of the users
    listOfContents: Vector<ContentModel> = new Vector<ContentModel>('list-of-contents');
    // Variable to save all the users inside the contract
    listOfUsers: Vector<UserModel> = new Vector<UserModel>('list-of-users');
    // Variable to save all the subscriptions inside the contract
    listOfSubscriptions: Vector<SubscriptionModel> = new Vector<SubscriptionModel>('list-of-subscriptions');

    // Get all users function
    @view({})
    get_users({from = 0, limit = 10}): UserModel[] {
        return this.listOfUsers.toArray().slice(from, from + limit);
    }

    // Get the profile for specific user
    @view({})
    get_profile({account_id}: { account_id: string }): UserModel {
        return this.listOfUsers.toArray().filter(user => user.account_id === account_id)[0]
    }

    // Check if specific handle is taken
    @view({})
    handle_exists({handle}: { handle: string }): Boolean {
        return this.listOfUsers.toArray().filter(user => user.handle === handle).length > 0
    }

    @view({})
    /**
     * Get All contents of the plattform with pagination
     * @param from
     * @param limit
     * @returns {UserModel}
     */
    get_all_contents({from = 0, limit = 10}): ContentModel[] {
        return this.listOfContents.toArray()
            .filter(course => course.is_available)
            .slice(from, from + limit);
    }

    @view({})
    get_user_content({from = 0, limit = 10, account_id}): ContentModel[] {
        return this.listOfContents.toArray()
            .filter(course => course.is_available && course.account_id === account_id)
            .slice(from, from + limit);
    }

    @view({})
    get_creator_subscriptions({from = 0, limit = 10, account_id}): SubscriptionModel[] {
        return this.listOfSubscriptions.toArray()
            .filter(subscription => subscription.creator === account_id)
            .slice(from, from + limit);
    }

    @call({})
    create_new_content({title, description, pictures, price}: {
        title: string,
        description: string,
        pictures: string[],
        price: number
    }): void {
        let lastId = 0
        if (this.listOfContents.length !== 0) {
            lastId = parseInt(this.listOfContents.get(this.listOfContents.length - 1).id)
        }
        const id = (lastId + 1).toString()
        const newContent = {
            id,
            title,
            description,
            pictures,
            price,
            is_available: true,
            account_id: near.signerAccountId()
        }
        this.listOfContents.push(newContent)
    }

    @call({})
    delete_content({id}: { id: string }): boolean {
        const index = this.listOfContents.toArray().findIndex(content => content.id === id)

        // Validate that creator owns the contract
        if (this.listOfContents.toArray()[index].account_id !== near.signerAccountId()) {
            return false
        }

        // Make a copy of the element that we want to set as inactive
        let newContent = {...this.listOfContents.toArray()[index]}
        newContent.is_available = false

        // Set inactive to delete from the list of available contents
        this.listOfContents.replace(index, newContent)

        return true
    }

    @call({})
    async create_new_user({handle, type}: {
        handle: string,
        type: string
    }): Promise<UserModel> {

        // Find id the user already exists
        const indexOriginalUser = this.listOfUsers.toArray().findIndex(user => user.account_id === near.signerAccountId())
        // // Set object for the new user
        let user = {
            handle,
            type,
            account_id: near.signerAccountId()
        }

        // If the user exists just update the original user
        if (indexOriginalUser !== -1) {
            this.listOfUsers.replace(indexOriginalUser, user)
        } else {
            this.listOfUsers.push(user)
        }
        return user
    }

    @call({})
    create_new_subscription({price, durationInMonths, name}: {
        name: string
        price: number,
        durationInMonths: number
    }): void {
        let lastId = 0
        if (this.listOfSubscriptions.length !== 0) {
            lastId = parseInt(this.listOfSubscriptions.get(this.listOfSubscriptions.length - 1).id)
        }

        const newSubscription = {
            id: (lastId + 1).toString(),
            name,
            price,
            durationInMonths,
            creator: near.signerAccountId()
        }

        this.listOfSubscriptions.push(newSubscription)
    }
}