
export class ContentModel {
  id: string;
  account_id: string;
  title: string;
  description: string;
  pictures: string[];
  price: number;
  is_available?: boolean;
  comments?: []
}