export class SubscriptionModel {
    id: string;
    name: string;
    creator: string;
    price: number;
    durationInMonths: number
}